#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>

#define Firmware "v1.0.0"

String AP_SSID = "networkScanner";
String AP_PASS = "Gonpa84ar";

/**
 * ENC_TYPES:
 * TKIP (WPA) = 2
 * WEP = 5
 * CCMP (WPA) = 4
 * NONE = 7
 * AUTO = 8
 */

void setup() {
  Serial.begin(115200);
  while(!Serial){}
  Serial.println();
  Serial.println("Serial comm initiated");
  WiFi.disconnect();
  WiFi.softAPdisconnect();
  Serial.println("WiFi desconectado");
  WiFi.mode(WIFI_AP_STA);
  WiFi.softAP(AP_SSID, AP_PASS, 1, false);
  Serial.println("WiFi mode: AP Station initiated");
  Serial.print("Dirección IP Soft-AP = "); Serial.println(WiFi.softAPIP());
  delay(500);
}

void ScanNetworks(){
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0)
    Serial.println("no networks found");
  else
  {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i)
     {
      // Print SSID and RSSI for each network found
      String Msg = "";
      int rssi = WiFi.RSSI(i);
      Msg += (i + 1);
      Msg += ": ";
      Msg += WiFi.SSID(i);
      Msg += " (";
      Msg += rssi;
      if(rssi >= -90){
        if(rssi >= -80){
          if(rssi >= -70){
            if(rssi >= -67){
              if(rssi >= -30){
                Msg += " - Wow!!";
              }
              else
              {
                Msg += " - Amazing";
              }
            }
            else
            {
              Msg += " - Very Good";
            }
          }
          else
          {
            Msg += " - Good";
          }
        }
        else
        {
          Msg += " - Poor";
        }
      }
      else
      {
        Msg += " - Unusable";
      }
      Msg += ")";
      Msg += WiFi.encryptionType(i);
      byte encryption = WiFi.encryptionType(i);
      Serial.println(Msg);
      delay(10);
    }
  }
}

void loop() {
  Serial.println("--- Network Scan ---");
  ScanNetworks();
  delay(10000);
}
